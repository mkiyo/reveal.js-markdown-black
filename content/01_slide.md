## List with Fragment

``` markdown
* item1 <!-- .element class="fragment" -->
* item2 <!-- .element class="fragment" -->
```

* item1 <!-- .element class="fragment" -->
* item2 <!-- .element class="fragment" -->

--

## Table

``` markdown
|  col1 |  col2 |  col3 |
| :---: | :---: | :---: |
|  val1 |  val2 |  val3 |
|  val4 |  val5 |  val6 |
```

|  col1 |  col2 |  col3 |
| :---: | :---: | :---: |
|  val1 |  val2 |  val3 |
|  val4 |  val5 |  val6 |

---

## Code Highlight

### C++ Sample Code

```` markdown
``` cpp:main.cpp [|1|3|5-6]
#include <iostream>

int main(int argc, char *argv[])
{
    std::cout << "Hello, world." << std::endl;
    return 0;
}
```
````

``` cpp:main.cpp [|1|3|5-6]
#include <iostream>

int main(int argc, char *argv[])
{
    std::cout << "Hello, world." << std::endl;
    return 0;
}
```

--

### Python Sample Code

```` markdown
``` python [|1-2|4|5|6]
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('data.csv')
res = df.groupby(by='key').mean()
res.to_csv('out.csv')
```
````

``` python [|1-2|4|5|6]
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('data.csv')
res = df.groupby(by='key').mean()
res.to_csv('out.csv')
```

---

## Math

### マルコフ (Markov) の不等式

``` markdown
任意の $X$ と $a \gt 0$ に対して $E[|X|]$ が成立とき、次式が成立する。

`$$\scriptsize{
P (|X| \ge a) \le \dfrac{E[|X|]}{a} \tag{1}
}$$`
```

任意の $X$ と $a \gt 0$ に対して $E[|X|]$ が成立とき、次式が成立する。

`$$\scriptsize{
P (|X| \ge a) \le \dfrac{E[|X|]}{a} \tag{1}
}$$`

4
