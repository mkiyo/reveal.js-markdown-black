import glob
import os
from string import Template
import shutil
import re
import gzip

dstDir = 'public'
srcDir = 'content'
templateDir = 'template'

#mkdir public
os.makedirs(dstDir, exist_ok=True)

#cp -r reveal.js/dist public
os.makedirs(f'{dstDir}/dist', exist_ok=True)
shutil.copytree('reveal.js/dist', f'{dstDir}/dist', dirs_exist_ok=True)

#cp -r reveal.js/plugin public
os.makedirs(f'{dstDir}/plugin', exist_ok=True)
shutil.copytree('reveal.js/plugin', f'{dstDir}/plugin', dirs_exist_ok=True)

with open(f'{templateDir}/template.html') as f:
    template = Template(f.read())

mds = []
for file in glob.glob(f'{dstDir}/*.md'):
    with open(file) as f:
        md = f.read()
    mds.append(md)

text = template.substitute({'md': '\n---\n'.join(mds)})
with open(f'{dstDir}/index.html', 'w') as f:
    f.write(text)

## to gzip
for p in glob.glob(f'{dstDir}/**/*', recursive=True):
    if re.search('\.(htm|html|css|js)$', str(p)):
        with open(p, 'rb') as i:
            with gzip.open(f'{p}.gz', 'w') as o:
                o.write(i.read())

