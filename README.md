# reveal.js-markdown-black

[https://mkiyo.gitlab.io/reveal.js-markdown-black](https://mkiyo.gitlab.io/reveal.js-markdown-black)

このリポジトリをクローンして

``` sh
git clone --recursive git@gitlab.com:mkiyo/reveal.js-markdown-black.git
```

``` sh
git clone git@gitlab.com:mkiyo/reveal.js-markdown-black.git # --recursiveを忘れた場合
git submodule update --init --recursive
```

content以下にMarkdownファイルを配置してください。
記載方法はcontent/01_sample.mdを参考にしてください。
詳細は[Revea.js](https://revealjs.com/)を。
